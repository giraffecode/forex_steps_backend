<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\News;
use App\NewsCategory;
use App\Ad;
use App\Status;

class NewsController extends Controller
{
    public function GetImportantNews(){
        $arr = array();
        $objNews = new News();
        $result = $objNews->getImportantNews();
        $arr['data'] = $result;
        $arr = Status::mergeStatus($arr,200);
        return $arr;
    }

    public function RelatedNews($id){
        $arr = array();
        $objNews = new News();
        $result = $objNews->relatedNews($id);
        $arr['data'] = $result;
        $arr = Status::mergeStatus($arr,200);
        return $arr;
    }

    public function RelatedNewsCategory($id){
        $arr = array();
        $objNewsCategory = new NewsCategory();
        $category = $objNewsCategory->categoryName($id);
        $objNews = new News();
        $relatedNews = $objNews->relatedNewsCategory($id);
        $objAd = new Ad();
        $relatedAds = $objAd->relatedAds($id);
        $arr['category'] = $category;
        $arr['news'] = $relatedNews;
        $arr['ad'] = $relatedAds;
        $arr = Status::mergeStatus($arr,200);
        return $arr;
    }

    public function NewsDetails($id){
        $arr = array();
        $objNews = new News();
        $objAd = new Ad();
        $newsDetails = $objNews->newsDetails($id);
        $relatedNews = $objNews->relatedNews($newsDetails[0]['news_categories_id']);
        $similarNews = $objNews->similarNews($newsDetails[0]["title"]);
        $newsAds = $objAd->relatedAds($newsDetails[0]['news_categories_id']);
        $arr['newsDetails'] = $newsDetails;
        $arr['relatedNews'] = $relatedNews;
        $arr['similarNews'] = $similarNews;
        $arr['newsAds'] = $newsAds;
        $arr = Status::mergeStatus($arr,200);
        return $arr;
    }

    public function SimilarNews($id){
        $arr = array();
        $objNews = new News();
        $newsDetails = $objNews->newsDetails($id);
        $result = $objNews->similarNews($newsDetails[0]["title"]);
        $arr['data'] = $result;
        $arr = Status::mergeStatus($arr,200);
        return $arr;
    }

    public function ListCategory(){
        $arr = array();
        $objNewsCategory = new NewsCategory();
        $result = $objNewsCategory->listCategory();
        $arr['data'] = $result;
        $arr = Status::mergeStatus($arr,200);
        return $arr; 
    }

    public function CategoriesAndNews(){
        $arr = array();
        $objNewsCategory = new NewsCategory();
        $arrCategory = $objNewsCategory->listCategory();
        foreach($arrCategory as $objCategory){
            $objNews = new News();
            $objAd = new Ad();
            $relatedNews = $objNews->relatedNews($objCategory['id']);
           
            $relatedAds = $objAd->relatedAds($objCategory['id']);
            $objCategory['relatedNews'] = $relatedNews;
            $objCategory['relatedAds'] = $relatedAds; 
        }
        $arr['data'] = $arrCategory;
        $arr = Status::mergeStatus($arr,200);
        return $arr; 
    }

    public function RelatedAds($id){
        $arr = array();
        $objAd = new Ad();
        $result = $objAd->relatedAds($id);
        $arr['data'] = $result;
        $arr = Status::mergeStatus($arr,200);
        return $arr;
    }
    
    public function NewsSameTag($tag){
        $arr = array();
        $objNews = new News();
        $result = $objNews->newsSameTag($tag);
        $arr['data'] = $result;
        $arr = Status::mergeStatus($arr,200);
        return $arr;
    }

    public function GetLatestNews(){
        $arr = array();
        $objNews = new News();
        $result = $objNews->getLatestNews();
        $arr['data'] = $result;
        $arr = Status::mergeStatus($arr,200);
        return $arr;
    }
}
