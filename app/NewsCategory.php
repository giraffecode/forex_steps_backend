<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NewsCategory extends Model
{
    protected $fillable = [
        'name'
    ];

    public function listCategory(){
        $result = NewsCategory::get();
        return $result;
    }
    
    public function categoryName($id){
        $result = NewsCategory::where('id', $id)->get();
        return $result;
    }

}
